import os
import slack
from flask import Flask, request, jsonify
import requests
import json

app = Flask(__name__)

token = os.environ["WIKI_GAME_BOT_TOKEN"]
client = slack.WebClient(token=token)

@app.route('/wikigame', methods=['POST'])
def WikiGame():
    pages = getWikipediaPages()
    message = getInitMessage(pages)
    client.chat_postEphemeral(
        channel=request.form['channel_id'], 
        user=request.form['user_id'], 
        blocks=message)
    return '', 200

@app.route('/wikigameinitaction', methods=['POST'])
def InitAction():
    actions = {
        "start": start,
        "shuffle": shuffle,
        "cancel": cancel
    }
    payload = json.loads(request.form['payload'])
    actionId = payload['actions'][0]['action_id']
    response = actions[actionId](payload)
    if response is not None:
        responseUrl = payload['response_url']
        requests.post(url=responseUrl, json=response)
    return '', 200

def start(payload):
    #media isnt unfurled in ephemeral messages, so repost
    message = payload['actions'][0]['value']
    user = payload['user']['name']
    client.chat_postMessage(
        channel=payload['channel']['id'], 
        text=f'{user} started a wiki game! {message}', 
        unfurl_links=True,
        unfurl_media=True)
    return {
        "delete_original": True
    }

def shuffle(payload):
    pages = getWikipediaPages()
    message = getInitMessage(pages)
    return {
        "blocks": message,
        "replace_original": True
    }

def cancel(payload):
    return {
        "delete_original": True
    }

def getWikipediaPages():
    uri = f'https://en.wikipedia.org/w/api.php'
    r = requests.get(f"{uri}?action=query&rnnamespace=0&format=json&list=random&rnlimit=2").json()
    pageIds = list(str(p['id']) for p in r['query']['random'])
    #slack tends to unfurl wikipedia urls better when you don't reference them by id
    r = requests.get(f"{uri}?action=query&prop=info&pageids={'|'.join(pageIds)}&inprop=url&format=json").json()
    result = []
    for pid in pageIds:
        p = r['query']['pages'][pid]
        title = p['title']
        fullUrl = p['fullurl']
        result.append(f'<{fullUrl}|{title}>')
    return result

def getInitMessage(pages):
    challengeText = f'{pages[0]} to {pages[1]}'
    return [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": challengeText
            }
        },
        {
            "type": "actions",
            "elements": [
                getButtonAction(text='Start', actionId="start", value=challengeText),
                getButtonAction(text='Shuffle', actionId="shuffle"),
                getButtonAction(text='Cancel', actionId="cancel", style='danger')
            ]
        }
    ]

def getButtonAction(text, actionId, style=None, value=None):
    result = {
        "type": "button",
        "text": {
            "type": "plain_text",
            "text": text
        },
        "action_id": actionId
    }

    if style is not None:
        result["style"] = style

    if value is not None:
        result["value"] = value
    
    return result